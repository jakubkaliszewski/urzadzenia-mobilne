import 'react-native-gesture-handler'
import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createStackNavigator } from 'react-navigation-stack';

import Icon from 'react-native-vector-icons/Fontisto';

import AboutScreen from './Screens/AboutScreen';
import HomeScreen from './Screens/HomeScreen';
import RemoteGalleryScreen from './Screens/RemoteGalleryScreen';
import ClickerScreen from './Screens/ClickerScreen';
import HelloWorldScreen from './Screens/HelloWorldScreen';
import AccelerometerBall from './Components/AccelerometerBall/AccelerometerBall';

import AuthLoadingScreen from './Screens/AuthLoadingScreen';

const iconColor = "#4F8EF7";

const ClickerNavigator = createStackNavigator({
  'Clicker': {
    screen: ClickerScreen
  }
}, {
  initialRouteName: 'Clicker',
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: 'white',
    },
    headerTintColor: 'black',
    headerTitleStyle: {
      fontWeight: 'bold',
      textAlign: 'center',
      flex: 1,
    },
  },
});

const HelloWorldNavigator = createStackNavigator({
  'HelloWorld': {
    screen: HelloWorldScreen
  }
}, {
  initialRouteName: 'HelloWorld',
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: 'white',
    },
    headerTintColor: 'black',
    headerTitleStyle: {
      fontWeight: 'bold',
      textAlign: 'center',
      flex: 1,
    },
  },
});

const RemoteGalleryNavigator = createStackNavigator({
  'RemoteGallery': {
    screen: RemoteGalleryScreen
  }
}, {
  initialRouteName: 'RemoteGallery',
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: 'white',
    },
    headerTintColor: 'black',
    headerTitleStyle: {
      fontWeight: 'bold',
      textAlign: 'center',
      flex: 1,
    },
  },
});



const HomeDraverNavigator = createDrawerNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      drawerIcon: () => (<Icon name="home" size={15} color={iconColor} />)
    }
  },
  "Hello World": {
    screen: HelloWorldNavigator,
    navigationOptions: {
      drawerIcon: () => (<Icon name="compass-alt" size={15} color={iconColor} />)
    }
  },
  "Clicker": {
    screen: ClickerNavigator,
    navigationOptions: {
      drawerIcon: () => (<Icon name="flash" size={15} color={iconColor} />)
    }
  },
  "Remote Gallery": {
    screen: RemoteGalleryNavigator,
    navigationOptions: {
      drawerIcon: () => (<Icon name="picture" size={15} color={iconColor} />)
    }
  },
  "Accelerometer Ball": {
    screen: AccelerometerBall,
    navigationOptions: {
      drawerIcon: () => (<Icon name="dribbble" size={15} color={iconColor} />)
    }
  }
});

const BottomTabNavigator = createBottomTabNavigator({
  Home: {
    screen: HomeDraverNavigator,
    navigationOptions: {
      tabBarIcon: () => (<Icon name="home" size={20} color={iconColor} />)
    }
  },
  About: {
    screen: AboutScreen,
    navigationOptions: {
      tabBarIcon: () => (<Icon name="info" size={20} color={iconColor} />)
    }
  },
});

const SwitchNavigator = createSwitchNavigator({
  AuthLoading: AuthLoadingScreen,
  App: BottomTabNavigator,    // tu podczepiamy aplikację z nawigacją
},{
    initialRouteName: 'AuthLoading',
});



const AppContainer = createAppContainer(SwitchNavigator);
class App extends React.Component {
  render() {
    return (
      <AppContainer />
    );
  }
}
export default App;