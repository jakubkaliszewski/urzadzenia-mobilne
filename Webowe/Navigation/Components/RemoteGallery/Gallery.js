import React from 'react';
import Header from './Header';
import RemoteGallery from './RemoteGallery';

export default class Gallery extends React.Component {
  render() {
    return (
      <>
        <Header title="Remote gallery"></Header>
        <RemoteGallery url="http://is.umk.pl/~kdobosz/pum/nebula-images.json"></RemoteGallery>
      </>
    );
  }
}