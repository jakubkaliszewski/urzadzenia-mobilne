import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, Image } from 'react-native';
import LoaderIndicator from './LoaderIndicator';
import NebulaImage from './NebulaImage';

export default class RemoteGallery extends Component {
    constructor(props) {
        super(props);
        this.state = {
            availableURLs: [],
            actualImageURL: '',
            loading: false,
            loadingList: true
        }
        
        this.getURLs(this.props.url).then(result => this.setState({ availableURLs: result, loadingList:false }));
    }

    async getURLs(source) {
        //pobrać listę z pliku json przy tworzeniu komponentu....
        let response = await fetch(source).then(result => { return result.json() });
        let results = [];
        for (let i = 0; i < response.images.length; i++) {
            results[i] = new NebulaImage(response.images[i].id, response.images[i].title, response.images[i].url);
        }

        return results;
    }

    onSelectItem(selected) {
        this.setState({ actualImageURL: selected });
    }


    render() {
        return (
            <>  
                {!this.state.loadingList &&
                <View style={{ flex: 2, marginTop: "2%" }} id='image'>
                    <Image source={{ uri: this.state.actualImageURL }} height="100%" width="100%"
                        resizeMode="cover" onLoadStart={() => this.setState({ loading: true })}
                        onLoadEnd={() => { this.setState({ loading: false }) }} />
                    {this.state.loading && <LoaderIndicator color="#00ffd2" />}
                </View>
                }


                <View id="list" style={{ flex: 1, marginTop: "2%" }}>
                    <FlatList
                        data={this.state.availableURLs}
                        renderItem={({ item }) => (
                            <TouchableOpacity onPress={() => this.onSelectItem(item.url)}>
                                <View style={{ padding: "2%", marginBottom: "2%" }}>
                                    <Text style={{ alignSelf: 'center' }} >{item.name}</Text>
                                </View>
                            </TouchableOpacity>
                        )}
                        keyExtractor={(item, index) => item.id}>
                    </FlatList>
                    {this.state.loadingList && <LoaderIndicator color="#00ffd2" />}
                </View>
            </>
        );
    }
}