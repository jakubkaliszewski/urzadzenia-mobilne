import React, { Component } from 'react';
import { View, Text } from 'react-native';

export default class Header extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={{
                backgroundColor: this.props.backgroundColor !== undefined ? this.props.backgroundColor : '',
                alignItems: 'center',
                justifyContent: 'center',
            }}>
                <Text style={{fontSize: 30, fontWeight: 'bold', padding: "2%"}}>{this.props.title !== undefined ? this.props.title : 'Remote Gallery Header'}</Text>
            </View>
        );
    }
}