import React, { Component } from 'react';
import { View, ActivityIndicator } from 'react-native';

export default class LoaderIndicator extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View {...this.props} style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                position: 'absolute',
                backgroundColor: this.props.backgroundColor !== undefined ? this.props.backgroundColor : 'black'
            }
            }>
                <ActivityIndicator size="large" color={this.props.color !== undefined ? this.props.color : 'red'} />
            </View>
        );
    }
}