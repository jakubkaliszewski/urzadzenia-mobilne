import {
    GoogleSignin,
    statusCodes,
} from 'react-native-google-signin';
import firebase from 'react-native-firebase';

export default class Auth{

    static configure(){
        GoogleSignin.configure({
            //It is mandatory to call this method before attempting to call signIn()
            scopes: ['https://www.googleapis.com/auth/drive.readonly'],
            // Repleace with your webClientId generated from Firebase console
            webClientId: '290057738398-8pgo472eakkr7q262oheh37uddp3nskn.apps.googleusercontent.com',
        });
    }

    static async isSignedIn(){
        let isSignedIn = await GoogleSignin.isSignedIn();
        return isSignedIn;
    }

    static async getCurrentUserInfo(){
        try {
            const userInfo = await GoogleSignin.signInSilently();
            console.log('User Info --> ', userInfo);
            return userInfo;
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_REQUIRED) {
                alert('User has not signed in yet');
                console.log('User has not signed in yet');
            } else {
                alert("Something went wrong. Unable to get user's info");
                console.log("Something went wrong. Unable to get user's info");
            }
        }
    };

    static async signIn(){
        //Prompts a modal to let the user sign in into your application.
        try {
            await GoogleSignin.hasPlayServices({
                //Check if device has Google Play Services installed.
                //Always resolves to true on iOS.
                showPlayServicesUpdateDialog: true,
            });
            const userInfo = await GoogleSignin.signIn();
            console.log('User Info --> ', userInfo);
            return userInfo;
        } catch (error) {
            console.log('Message', error.message);
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                console.log('User Cancelled the Login Flow');
            } else if (error.code === statusCodes.IN_PROGRESS) {
                console.log('Signing In');
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                console.log('Play Services Not Available or Outdated');
            } else {
                console.log('Some Other Error Happened');
            }
        }
    };

    static async signOut(){
        //Remove user session from the device.
        try {
            await GoogleSignin.revokeAccess().then(GoogleSignin.signOut());
        } catch (error) {
            console.error(error);
        }
    };

    static async firebaseLogSignIn(userInfo){
        if(userInfo !== null){
            try {
                const credential = firebase.auth.GoogleAuthProvider.credential(data.idToken, data.accessToken)
                const firebaseUserCredential = firebase.auth().signInWithCredential(credential);
            } catch (error) {
                console.error(error);
            }
        }
    }

    static async firebaseLogSignOut() {
        if (userInfo !== null) {
            try {
                await firebase.auth().signOut();
            } catch (error) {
                console.error(error);
            }
        }
    }
}