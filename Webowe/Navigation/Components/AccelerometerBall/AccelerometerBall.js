import React, { Component } from 'react';
import { View, StyleSheet, Animated } from 'react-native';
import { accelerometer, setUpdateIntervalForType, SensorTypes } from 'react-native-sensors';
import Screen, {SIZE} from './Screen';
import Ball, { BALL_SIZE } from './Ball';

export default class AccelerometerBall extends Component {
    constructor(props) {
        super(props);
        this.state = {
            position: { x: 0, y: 0 },
        }
    }
    async componentDidMount() {
        setUpdateIntervalForType(SensorTypes.accelerometer, 16);
        this.subscribe = accelerometer.subscribe(({ x, y, z, timestamp }) => {
            let newValue = {
                x: this.state.position.x - x, y: this.state.position.y + y
            }
            if(!this.hasFallenFromScreen(newValue))
                this.setState({ position: newValue });
        });
    }

    async componentWillUnmount() {
        this.subscribe = null;
    }
    
    hasFallenFromScreen(value){
        return Math.abs(value.x) > SIZE.width / 2 - BALL_SIZE/2 || Math.abs(value.y) > SIZE.height / 2 - BALL_SIZE/2;
    }
        

    render() {
        return (
            <>
                <Screen>
                    <Ball x={this.state.position.x} y={this.state.position.y} />
                </Screen>
            </>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    },
    img: {
        width: '100px',
        height: '100px',
        position: 'absolute',
        left: 0,
        top: 0,
    },
    imgr: {
        width: '50%',
        position: 'absolute',
        right: 0,
    }
});