import React from "react";
import { StyleSheet, View, Dimensions } from "react-native";

export const SIZE = Dimensions.get("screen");

export default function Screen({ children }) {
    return <View style={[styles.screen, styles.centeredContent]}>{children}</View>;
}

const styles = StyleSheet.create({
    screen: {
        alignItems: "center",
        backgroundColor: "rgb(0,0,0)",
        flexDirection: "column",
        height: SIZE.height,
        justifyContent: "center",
        width: SIZE.width
    }
});