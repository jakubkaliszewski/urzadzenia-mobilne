import React from 'react';
import {
    View,
    Text,
    StatusBar,
    Button,
    FlatList,
    Alert,
    Image,
    ToastAndroid,
    TouchableOpacity,
} from 'react-native';

export default class HelloWorld extends React.Component {
    constructor() {
        super();
        this.appName = 'Hello world';
        this.instructions = "Treść komunikatu bardzo bogata jest";
        this.listData = [
            { key: '1', value: 'Jakub' },
            { key: '2', value: 'Katarzyna' },
            { key: '3', value: 'Bogumił' },
            { key: '4', value: 'Bazyl' },
            { key: '5', value: 'Mateusz' },
            { key: '6', value: 'Jarek' },
            { key: '7', value: 'Bonifacy' },
            { key: '8', value: 'Heszke w Leszke' },
        ];
    }
    Item({ title, id }) {
        console.log(title);
        return (
            <TouchableOpacity onPress={() => {
                ToastAndroid.show('Mój klucz to: ' + id, ToastAndroid.LONG);
            }}>
                <Text style={{ padding: '2%', fontSize: 20 }}>{title}</Text>
            </TouchableOpacity>
        );
    }

    render() {
        return (
            <>
                <StatusBar barStyle="default-content" />
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <View style={{ flex: 1, backgroundColor: '#6a2', alignItems: 'center' }}>
                        <Text style={{ fontSize: 20, paddingBottom: '5%' }}>{this.appName}</Text>
                        <View style={{ width: 150 }}>
                            <Button title={"Button"} onPress={() => Alert.alert('Komunikat', this.instructions, [
                                {
                                    text: "Okeyka",
                                    style: { backgroundColor: "#00e622" }
                                },
                                {
                                    text: "Poniechaj",
                                    style: { backgroundColor: "#e63500" }
                                }
                            ], { cancelable: false }
                            )} />
                        </View>
                    </View>
                    <View style={{ flex: 3, backgroundColor: '#F3F' }}>
                        <Image style={{ flex: 1 }} source={{ uri: 'https://facebook.github.io/react/logo-og.png' }} />
                    </View>
                    <View style={{ flex: 2, backgroundColor: '#e1ab00', alignItems: 'center' }}>
                        <FlatList
                            data={this.listData}
                            renderItem={({ item }) => {
                                return(
                                <TouchableOpacity onPress={() => {
                                    ToastAndroid.show('Mój klucz to: ' + item.key, ToastAndroid.LONG);
                                }}>
                                    <Text style={{ padding: '2%', fontSize: 20 }}>{item.value}</Text>
                                </TouchableOpacity>
                                );
                            }}
                            keyExtractor={item => item.key}></FlatList>
                    </View>
                </View>
            </>
        );
    }
}