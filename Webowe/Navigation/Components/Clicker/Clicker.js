import React, { Component } from 'react';
import { StyleSheet, View, Text, FlatList, TouchableOpacity, TouchableHighlight } from 'react-native';
import MyButton from '../Shared/MyButton';

export default class Clicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            clicks: 0,
            containerColor: this.getRandomColor(),
            listContainerColor: ''
        };
        this.namedColors = [
            { key: '1', value: 'orange' },
            { key: '2', value: 'palegreen' },
            { key: '3', value: 'plum' },
            { key: '4', value: 'olive' },
            { key: '5', value: 'magenta' },
            { key: '6', value: 'aqua' },
            { key: '7', value: 'tomato' },
            { key: '8', value: 'silver' },
            { key: '9', value: 'yellow' },
            { key: '10', value: 'saddlebrown' },
        ];
    }

    getRandomColor() {
        let red = this.getRandomIntInclusive(0, 255);
        let green = this.getRandomIntInclusive(0, 255);
        let blue = this.getRandomIntInclusive(0, 255);

        return 'rgb(' + red + ',' + green + ',' + blue + ')';
    }

    getRandomIntInclusive(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    onSelectItem(selected) {
        this.setState({ listContainerColor: selected });
    }



    render() {
        return (
            <>
                <View id="clickersInfo" style={{ flex: 2 }}>
                    <Text style={styles.counter}>{this.state.clicks}</Text>
                    <View id="buttons" style={{ flexDirection: 'row', justifyContent: 'space-around', borderRadius: 15 }}>
                        <MyButton title="Click - 1" color='red' onPress={() => this.setState(previousState => ({ clicks: previousState.clicks - 1 }))}></MyButton>
                        <MyButton title="Click + 1" color='#32ff32' onPress={() => this.setState(previousState => ({ clicks: previousState.clicks + 1 }))}></MyButton>
                    </View>
                </View>
                <TouchableOpacity id="colors" style={{ flex: 3 }} onPress={() => this.setState({ containerColor: this.getRandomColor() })} onLongPress={() => this.setState({ containerColor: 'white' })}>
                    <View style={{ flex: 1, marginTop: "10%", backgroundColor: this.state.containerColor, borderRadius: 10, }}>
                    </View>
                </TouchableOpacity>
                <View id="list" style={{ flex: 4, marginTop: "2%" }}>
                    <FlatList style={{ backgroundColor: this.state.listContainerColor, padding: "15%" }}
                        data={this.namedColors}
                        renderItem={({ item }) => (
                            <TouchableHighlight onPress={() => this.onSelectItem(item.value)}>
                                <View style={{ backgroundColor: item.value, padding: "5%" }}>
                                    <Text style={{ alignSelf: 'center', fontSize: 20, color: 'black' }} >{item.value}</Text>
                                </View>
                            </TouchableHighlight>
                        )}
                        keyExtractor={item => item.id}>
                    </FlatList>
                </View>
            </>
        );
    }

}

const styles = StyleSheet.create({
    title: {
        fontSize: 30,
        fontWeight: 'bold',
        alignSelf: "center",
        marginTop: "4%"
    },
    counter: {
        fontSize: 40,
        fontWeight: 'bold',
        alignSelf: "center",
        color: "#1da1f2",
        margin: "2%",
        marginTop: 0,
        padding: "2%"
    },
});