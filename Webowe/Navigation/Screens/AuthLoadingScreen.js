import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Alert,
    Image,
    ActivityIndicator,
    TouchableOpacity,
} from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Auth from '../Components/AuthLoading/Auth';

import {
    GoogleSigninButton,
} from 'react-native-google-signin';

export default class AuthLoadingScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userInfo: null,
            gettingLoginStatus: true,
        };
    }

    async componentDidMount() {
        await Auth.configure();
        let result = await Auth.isSignedIn();
        console.log('result:' + result);
        if(result === false)
        { 
            this.setState({ gettingLoginStatus: false });
            await this.signIn();
        }      
        else
            this.setState({ userInfo: result });

        console.log(this.state.userInfo);
        
        await this.props.navigation.navigate('App');
    }

    async signIn(){
        let user = await Auth.signIn();
        this.setState({ userInfo: user });
        await this.props.navigation.navigate('App');
    };

    async signOut(){
        //Remove user session from the device.
        try {
            Auth.signOut()
            this.setState({ userInfo: null }); // Remove the user from your app's state as well
        } catch (error) {
            console.error(error);
        }
    };

    render() {
        //returning Loader untill we check for the already signed in user
        if (this.state.gettingLoginStatus) {
            return (
                <View style={styles.container}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            );
        } else {
            if (this.state.userInfo != null) {
                //Showing the User detail
                return (
                    <View style={styles.container}>
                        <Image
                            source={{ uri: this.state.userInfo.user.photo }}
                            style={styles.imageStyle}
                        />
                        <Text style={styles.text}>
                            Name: {this.state.userInfo.user.name}{' '}
                        </Text>
                        <Text style={styles.text}>
                            Email: {this.state.userInfo.user.email}
                        </Text>
                        <TouchableOpacity style={styles.button} onPress={this._signOut}>
                            <Text>Logout</Text>
                        </TouchableOpacity>
                    </View>
                );
            } else {
                //For login showing the Signin button
                return (
                    <View style={styles.container}>
                        <GoogleSigninButton
                            style={{ width: 312, height: 48 }}
                            size={GoogleSigninButton.Size.Wide}
                            color={GoogleSigninButton.Color.Light}
                            onPress={()=> Auth.signIn()}
                        />
                    </View>
                );
            }
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    imageStyle: {
        width: 200,
        height: 300,
        resizeMode: 'contain',
    },
    button: {
        alignItems: 'center',
        backgroundColor: '#DDDDDD',
        padding: 10,
        width: 300,
        marginTop: 30,
    },
});