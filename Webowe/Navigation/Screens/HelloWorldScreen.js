import React from 'react';
import { View } from 'react-native';
import HelloWorld from '../Components/HelloWorld/HelloWorld';

export default class HelloWorldScreen extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        title: 'Hello world',
        headerLeft: (<View></View>),
    });

    render() {
        return (
            <>
                <HelloWorld />
            </>
        );
    }
}