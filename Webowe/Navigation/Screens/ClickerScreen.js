import React from 'react';
import { View } from 'react-native';
import Clicker from '../Components/Clicker/Clicker';

export default class ClickerScreen extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        title: 'Clicker',
        headerLeft: (<View></View>)
    });

    render() {
        return (
            <>
                <Clicker />
            </>
        );
    }
}