import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Gallery from '../Components/RemoteGallery/Gallery';

export default class RemoteGalleryScreen extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        title: 'Remote Gallery',
        headerLeft: (<View></View>),
        headerRight: (
            <View>
                <TouchableOpacity onPress={() => navigation.navigate('Details')}>

                </TouchableOpacity>
            </View>
        ),
    });

    render() {
        return (
            <>
                <Gallery />
            </>
        );
    }
}

export class RemoteGalleryDetails extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text>Gallery Details</Text>
            </View>
        );
    }
}