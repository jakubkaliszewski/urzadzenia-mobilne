import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';


export default class MyButton extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <TouchableOpacity {...this.props}>
                <View
                    style={{
                        backgroundColor: this.props.color !== undefined ? this.props.color : 'black',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: 15,
                        padding: 15,
                    }}>
                    <Text style={{ color: 'white', fontSize: 20, fontWeight: '800' }}>
                        {this.props.title}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }
}