import React, {Component} from 'react';
import { View } from 'react-native';
import Clicker from './Clicker';

class App extends Component {
  render() {
    return (
    <View style={{ flex: 1, margin:'1%' }}>
      <Clicker appName='Clicker'/>
    </View>
    );
  }
};

export default App;