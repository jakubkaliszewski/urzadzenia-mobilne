import React, { Component } from 'react';
import HelloWorld from './HelloWorld';

class App extends Component {
  render() {
    return (
      <>
        <HelloWorld style={{ margin: "2%" }}></HelloWorld>
      </>
    );
  }
}

export default App;