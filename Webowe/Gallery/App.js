import React, { Component } from 'react';
import Gallery from './Gallery';

class App extends Component {
  render() {
    return (
      <>
        <Gallery style={{margin:"2%"}}></Gallery>
      </>
    );
  }
}

export default App;
