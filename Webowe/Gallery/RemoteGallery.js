import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, Image } from 'react-native';
import LoaderIndicator from './LoaderIndicator';
import NebulaImage from './NebulaImage';

export default class RemoteGallery extends Component {
    constructor(props) {
        super(props);
        this.state = {
            actualImageURL: '',
            availableURLs: [],
            loading: false
        }
    }

    componentDidMount() {
        this.getURLs(this.props.url).then(result => this.setState({availableURLs: result}));
    }

    async getURLs(source){
        //pobrać listę z pliku json przy tworzeniu komponentu....
        let response = await fetch(source);
        let result = await response.json();
        let results = [];
        for (let i = 0; i < result.images.length; i++) {
            results[i] = new NebulaImage(result.images[i].id, result.images[i].title, result.images[i].url);
        }

        return results;
    }

    onSelectItem(selected) {
        this.setState({ actualImageURL: selected });
    }


    render() {
        return (
            <>
                <View style={{ flex:2, marginTop: "2%"}} id='image'>
                    <Image source={{ uri: this.state.actualImageURL }} height="100%" width="100%"
                        resizeMode="cover" onLoadStart={() => this.setState({ loading: true })}
                        onLoadEnd={() => {this.setState({ loading: false })}} />
                    {this.state.loading && <LoaderIndicator color="#00ffd2"/>}
                </View>


                <View id="list" style={{ flex: 1, marginTop: "2%" }}>
                    <FlatList
                        data={this.state.availableURLs}
                        renderItem={({ item }) => (
                            <TouchableOpacity onPress={() => this.onSelectItem(item.url)}>
                                <View style={{padding: "2%", marginBottom: "2%" }}>
                                    <Text style={{ alignSelf: 'center'}} >{item.name}</Text>
                                </View>
                            </TouchableOpacity>
                        )}
                        keyExtractor={(item, index) => item.id}>
                    </FlatList>
                </View>
            </>
        );
    }
}